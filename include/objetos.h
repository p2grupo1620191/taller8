//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo;

//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas

typedef struct obj1{
  int nota;
  int suma;
  int contador;
  float promedio;
} Obj1;

void crear_Obj1(void *ref, size_t tamano);
void destruir_Obj1(void *ref, size_t tamano);

typedef struct estudiante{
  char nombre[25];
  char observaciones[150];
  int asistencia;
  int notas;
} Estudiante;

void crear_Estudiante(void *ref, size_t tamano);
void destruir_Estudiante(void *ref, size_t tamano);

typedef struct cliente{
  char nombre[25];
  char direccion[75];
  long numero;
} Cliente;

void crear_Cliente(void *ref, size_t tamano);
void destruir_Cliente(void *ref, size_t tamano);
