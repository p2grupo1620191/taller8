#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"
#include "slaballoc.h"

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
    SlabAlloc *prueba = (SlabAlloc *)malloc(sizeof(SlabAlloc));
    prueba->mem_ptr=(void *)malloc(TAMANO_CACHE*tamano_objeto);
    prueba->nombre=nombre;
    prueba->tamano_cache=TAMANO_CACHE;
    prueba->tamano_objeto=tamano_objeto;
    prueba->constructor=constructor;
    prueba->destructor=destructor;
    int i=0;
    Slab *slabs=malloc(prueba->tamano_cache*sizeof(Slab));
    for(i=0;i<prueba->tamano_cache;i++){
        slabs[i].ptr_data=prueba->mem_ptr + i*(prueba->tamano_objeto);
        slabs[i].ptr_data_old=NULL;
prueba->constructor(prueba->mem_ptr + i*(prueba->tamano_objeto),prueba->tamano_objeto);
        slabs[i].status=DISPONIBLE;}
    prueba->slab_ptr=slabs;
    return prueba;
    }

void *obtener_cache(SlabAlloc *alloc, int crecer){
  void *retornar=NULL;
  if(crecer==1 && alloc->cantidad_en_uso<alloc->tamano_cache){retornar = NULL;}
  
  else if(alloc->cantidad_en_uso<alloc->tamano_cache){
    int i=0;
    while(alloc->slab_ptr[i].status!=DISPONIBLE){i++;}
      alloc->slab_ptr[i].status=EN_USO;
      alloc->cantidad_en_uso+=1;
      retornar = (void *)alloc->mem_ptr + i*(alloc->tamano_objeto);}
  
  else if(crecer==1){
    unsigned int i=0;

    Slab *slabs = alloc->slab_ptr;
    for(i=0;i<alloc->tamano_cache;i++){
    slabs[i].ptr_data_old=slabs[i].ptr_data;}

    alloc->mem_ptr =realloc(alloc->mem_ptr,2*alloc->tamano_cache);
    alloc->tamano_cache=2*alloc->tamano_cache;
    slabs=realloc(alloc->slab_ptr,alloc->tamano_cache*sizeof(Slab));
    for(i=0;i<alloc->tamano_cache;i++){
      if(i>=alloc->cantidad_en_uso){
        slabs[i].ptr_data=alloc->mem_ptr + i*(alloc->tamano_objeto);
        slabs[i].ptr_data_old=NULL;
        alloc->constructor((void *)(alloc->mem_ptr+i*(alloc->tamano_objeto)),alloc->tamano_objeto);
        slabs[i].status=DISPONIBLE;}
      else{
        slabs[i].ptr_data=alloc->mem_ptr + i*(alloc->tamano_objeto);
      }}
    alloc->slab_ptr=slabs;
    alloc->slab_ptr[alloc->cantidad_en_uso].status=EN_USO;
    alloc->cantidad_en_uso +=1;
    retornar = (void *)(alloc->mem_ptr + alloc->cantidad_en_uso - 1);}
    return retornar;}

void devolver_cache(SlabAlloc *alloc, void *obj){
    for(int i=0;i<alloc->tamano_cache;i++){
        if(alloc->slab_ptr[i].ptr_data==obj){
            alloc->slab_ptr[i].status=DISPONIBLE;
        }
    }
}

void destruir_cache(SlabAlloc *cache){
    if(cache->cantidad_en_uso==0){
      for(int i=0;i<cache->tamano_cache;i++){
        cache->destructor((void *)(cache->mem_ptr+i),cache->tamano_objeto);}
        free(cache->mem_ptr);
        free(cache->slab_ptr);
        free(cache);   
    }    
}

void stats_cache(SlabAlloc *cache){
    printf("Nombre de cache:\t%p\n",cache->nombre);
    printf("Cantidad de slabs:\t%d\n",cache->tamano_cache);
    printf("Cantidad de slabs en uso:\t%d\n",cache->cantidad_en_uso);
    printf("Tamano de objeto:\t%zx\n",cache->tamano_objeto);
    for(int i=0;i<cache->tamano_cache;i++){
    printf("Direccion ptr[%d].ptr_data:\t%p\t%i, ptr[%d].ptr_data_old: %p\n",i,cache->slab_ptr[i].ptr_data,cache->slab_ptr[i].status,i,cache->slab_ptr[i].ptr_data_old);   
}}
