all: bin/estatico bin/dinamico

bin/estatico: obj/prueba.o include/libslaballoc.a
	gcc -static -o bin/estatico  obj/prueba.o -L. include/libslaballoc.a

bin/dinamico: obj/prueba.o include/libslaballoc.so
	gcc include/libslaballoc.so -L include  obj/prueba.o include/libslaballoc.so -o bin/dinamico

obj/prueba.o: src/prueba.c
	gcc -Wall -c -Iinclude/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o

obj/funciones.o: src/funciones.c
	  gcc -Wall -c -Iinclude/ src/funciones.c -o obj/funciones.o

include/libslaballoc.a: obj/Ejemplo.o obj/funciones.o
	ar rcs include/libslaballoc.a obj/Ejemplo.o obj/funciones.o


include/libslaballoc.so: obj/Ejemplo.o obj/funciones.o
	gcc -shared -o include/libslaballoc.so  obj/Ejemplo.o obj/funciones.o 

.PHONY: clean
clean:
	rm bin/* obj/*.o lib/*

